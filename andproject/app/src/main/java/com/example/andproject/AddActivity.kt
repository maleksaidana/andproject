package com.example.andproject

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.view.Window
import kotlinx.android.synthetic.main.activity_ranking.*
import kotlinx.android.synthetic.main.header_lyt.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.mutableListOf
import kotlin.collections.toTypedArray

class AddActivity : Activity() {



    var HttpURL = "https://bibliobio.000webhostapp.com/andproject/activities.php"

    var finalResult: String? = null
    var hashMap: HashMap<String, String> = HashMap()
    var httpParse = HttpParse()
    val list = mutableListOf<activity>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_add);
        UserLoginFunction()


        /*menu*/
        val usermail: SharedPreferences = getSharedPreferences("user", 0)
        if (usermail.getString("mail", "")!="") {
            login.setImageResource(R.drawable.profile);
        }


        home.setOnClickListener  {startActivity(Intent(this, MainActivity::class.java))}
        AddActivity.setOnClickListener  {startActivity(Intent(this, com.example.andproject.AddActivity::class.java))}
        ScanProduct.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ScanProduct::class.java))}
        rankingbtn.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ranking::class.java))}
        login.setOnClickListener  {
            startActivity(Intent(this, Login::class.java))
            if (usermail.getString("mail", "")!=""){
                val intent = Intent(this, Profile::class.java)
                startActivity(intent)
            }
            else {
                startActivity(Intent(this, Login::class.java))
            }
        }
        /*menu*/

    }
    override fun onPause() {
        super.onPause()
        startService(Intent(this, StepService::class.java))

    }

    override fun onResume() {
        super.onResume()
        stopService(Intent(this, StepService::class.java))

    }

    override fun onDestroy() {
        super.onDestroy()
        startService(Intent(this, StepService::class.java))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

    }

    override fun onRestoreInstanceState(inState: Bundle) {
        super.onRestoreInstanceState(inState)
    }
    fun UserLoginFunction() {
        class UserLoginClass :
            AsyncTask<String?, Void?, String>() {
            override fun onPreExecute() {
                super.onPreExecute()
                //progressDialog =
                // ProgressDialog.show(this, "Loading Data", null, true, true)
            }

            override fun onPostExecute(activities: String) {
                super.onPostExecute(activities)



                for (i in activities.split("/").toTypedArray()) {

                 if(i.contains("?"))
                    list.add(activity(i.split("?").toTypedArray()[0],i.split("?").toTypedArray()[1]))
                }



                val myAdapter =activity_adapter(this@AddActivity, R.layout.activity_items,list as ArrayList<activity>?)
                simpleGridView.setAdapter(myAdapter)

            }

            protected override fun doInBackground(vararg params: String?): String? {

                finalResult = httpParse.postRequest(hashMap, HttpURL)
                return finalResult!!
            }
        }

        val userLoginClass = UserLoginClass()
        userLoginClass.execute()
    }

}
