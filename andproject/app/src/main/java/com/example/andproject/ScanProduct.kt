package com.example.andproject

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_scan_product.*
import kotlinx.android.synthetic.main.header_lyt.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*


class ScanProduct : Activity() {


    private val CAMERA_REQUEST = 1888
    private var imageView: ImageView? = null
    private val MY_CAMERA_PERMISSION_CODE = 100
    var HttpURL = "https://fr-en.openfoodfacts.org/cgi/product.pl"
    val list = mutableListOf<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(com.example.andproject.R.layout.activity_scan_product);

       button.setOnClickListener{
            if (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                } else {
                    TODO("VERSION.SDK_INT < M")
                }
            ) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        arrayOf(Manifest.permission.CAMERA),
                        MY_CAMERA_PERMISSION_CODE
                    )
                }
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            }
        }

        



        /*menu*/
        val usermail: SharedPreferences = getSharedPreferences("user", 0)
        if (usermail.getString("mail", "") != "") {
            login.setImageResource(com.example.andproject.R.drawable.profile);
        }


        home.setOnClickListener { startActivity(Intent(this, MainActivity::class.java)) }
        AddActivity.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    com.example.andproject.AddActivity::class.java
                )
            )
        }
        ScanProduct.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    com.example.andproject.ScanProduct::class.java
                )
            )
        }
        rankingbtn.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    com.example.andproject.ranking::class.java
                )
            )
        }
        login.setOnClickListener {
            startActivity(Intent(this, Login::class.java))
            if (usermail.getString("mail", "") != "") {
                val intent = Intent(this, Profile::class.java)
                startActivity(intent)
            } else {
                startActivity(Intent(this, Login::class.java))
            }
        }
        /*menu*/


        checkbarcode.setOnClickListener({
            UserLoginFunction(barcode.text.toString())
        })



    }

    override fun onPause() {
        super.onPause()
        startService(Intent(this, StepService::class.java))

    }

    override fun onResume() {
        super.onResume()
        stopService(Intent(this, StepService::class.java))

    }

    override fun onDestroy() {
        super.onDestroy()
        startService(Intent(this, StepService::class.java))
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show()
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent
    ) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            val photo = data.extras!!["data"] as Bitmap

            val uri = bitmapToFile(photo)




          //  Toast.makeText(this, path, Toast.LENGTH_LONG).show()
            UserLoginFunction(uri.toString())

            /*val document: Document? = Jsoup.connect(HttpURL)
                .data("file", path, FileInputStream(path))
                .post()*/



        }
    }
    private fun bitmapToFile(bitmap:Bitmap): Uri {
        // Get the context wrapper
        val wrapper = ContextWrapper(applicationContext)

        // Initialize a new file instance to save bitmap object
        var file = wrapper.getDir("Images",Context.MODE_PRIVATE)
        file = File(file,"${UUID.randomUUID()}.jpg")

        try{
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream)
            stream.flush()
            stream.close()
        }catch (e: IOException){
            e.printStackTrace()
        }

        // Return the saved bitmap uri
        return Uri.parse(file.absolutePath)
    }
    fun UserLoginFunction(Fname: String?) {
        class UserLoginClass :
            AsyncTask<String?, Void?, String>() {
            override fun onPreExecute() {
                super.onPreExecute()
                //progressDialog =
                // ProgressDialog.show(this, "Loading Data", null, true, true)
            }

            override fun onPostExecute(httpResponseMsg: String) {
                super.onPostExecute(httpResponseMsg)
                //progressDialog!!.dismiss()




                val intent = Intent(this@ScanProduct, product_info::class.java)
                intent.putExtra("html",httpResponseMsg)
                startActivity(intent)


            }

            protected override fun doInBackground(vararg params: String?): String? {



              // val doc = Jsoup.connect( HttpURL).get()

                val file = File(params[0])

               val document: Document? = Jsoup.connect(HttpURL)

                   .data("code", params[0])
                   .post()
                var tbody:Elements = document?.select("tr")!!

                var a :String =""





                return document.select("h1")[0].text()
            }
        }

        val userLoginClass = UserLoginClass()
        userLoginClass.execute(Fname)

    }

}
