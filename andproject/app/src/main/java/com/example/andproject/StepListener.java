package com.example.andproject;

public interface StepListener {

    public void step(long timeNs);

}