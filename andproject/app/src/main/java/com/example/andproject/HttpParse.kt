package com.example.andproject

import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder

class HttpParse {
    var FinalHttpData = ""
    var Result: String? = null
    var bufferedWriter: BufferedWriter? = null
    var outputStream: OutputStream? = null
    var bufferedReader: BufferedReader? = null
    var stringBuilder = StringBuilder()
    var url: URL? = null
    fun postRequest(
        Data: HashMap<String, String>,
        HttpUrlHolder: String?): String {
        try {
            url = URL(HttpUrlHolder)
            val httpURLConnection: HttpURLConnection = url!!.openConnection() as HttpURLConnection
            httpURLConnection.setReadTimeout(14000)
            httpURLConnection.setConnectTimeout(14000)
            httpURLConnection.setRequestMethod("POST")
            httpURLConnection.setDoInput(true)
            httpURLConnection.setDoOutput(true)
            outputStream = httpURLConnection.getOutputStream()
            bufferedWriter = BufferedWriter(
                OutputStreamWriter(outputStream, "UTF-8")
            )
            bufferedWriter!!.write(FinalDataParse(Data))
            bufferedWriter!!.flush()
            bufferedWriter!!.close()
            this.outputStream!!.close()

                bufferedReader = BufferedReader(
                    InputStreamReader(
                        httpURLConnection.getInputStream()
                    )
                )
                FinalHttpData = bufferedReader!!.readLine()

        } catch (e: Exception) {
            e.printStackTrace()
            FinalHttpData=e.toString()

        }
        return FinalHttpData
    }

    @Throws(UnsupportedEncodingException::class)
    fun FinalDataParse(hashMap2: HashMap<String, String>): String {
        for ((key, value) in hashMap2) {
            stringBuilder.append("&")
            stringBuilder.append(URLEncoder.encode(key, "UTF-8"))
            stringBuilder.append("=")
            stringBuilder.append(URLEncoder.encode(value, "UTF-8"))
        }
        Result = stringBuilder.toString()
        return Result as String
    }
}

