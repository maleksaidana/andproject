package com.example.andproject

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.view.Window
import kotlinx.android.synthetic.main.activity_ranking.*
import kotlinx.android.synthetic.main.header_lyt.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ranking : Activity() {



    var HttpURL = "https://bibliobio.000webhostapp.com/andproject/topwalkers.php"

    var finalResult: String? = null
    var hashMap: HashMap<String, String> = HashMap()
    var httpParse = HttpParse()
    val list = mutableListOf<walkers>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_ranking)
        UserLoginFunction()


        /*menu*/
        val usermail: SharedPreferences = getSharedPreferences("user", 0)
        if (usermail.getString("mail", "")!="") {
            login.setImageResource(R.drawable.profile);
        }


        home.setOnClickListener  {startActivity(Intent(this, com.example.andproject.MainActivity::class.java))}
        AddActivity.setOnClickListener  {startActivity(Intent(this, com.example.andproject.AddActivity::class.java))}
        ScanProduct.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ScanProduct::class.java))}
        rankingbtn.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ranking::class.java))}
        login.setOnClickListener  {
            startActivity(Intent(this, Login::class.java))
            if (usermail.getString("mail", "")!=""){
                val intent = Intent(this, Profile::class.java)
                startActivity(intent)
            }
            else {
                startActivity(Intent(this, Login::class.java))
            }
        }
        /*menu*/
    }
    override fun onPause() {
        super.onPause()
        startService(Intent(this, StepService::class.java))

    }

    override fun onResume() {
        super.onResume()
        stopService(Intent(this, StepService::class.java))

    }

    override fun onDestroy() {
        super.onDestroy()
        startService(Intent(this, StepService::class.java))
    }



    fun UserLoginFunction() {
        class UserLoginClass :
            AsyncTask<String?, Void?, String>() {
            override fun onPreExecute() {
                super.onPreExecute()
                //progressDialog =
                // ProgressDialog.show(this, "Loading Data", null, true, true)
            }

            override fun onPostExecute(activities: String) {
                super.onPostExecute(activities)


                var  count:Int =-1
                var day =""
                var month=""
                var year= ""
                var date=""
                for (i in activities.split("/").toTypedArray()) {
                    count += 1
                    if(i.contains("?"))
                    {
                        day = (i.split("?").toTypedArray()[1]).split("-").toTypedArray()[0]
                        month = (i.split("?").toTypedArray()[1]).split("-").toTypedArray()[1]
                        year = (i.split("?").toTypedArray()[1]).split("-").toTypedArray()[2]
                        val c1: Calendar = Calendar.getInstance()
                        c1.set(Calendar.MONTH,month.toInt()-1)
                        month= c1.getDisplayName(Calendar.MONTH, Calendar.LONG,
                            Locale.getDefault())
                        date = year+" "+month+" "+day

                        list.add(walkers(count.toString(),i.split("?").toTypedArray()[0],date,i.split("?").toTypedArray()[2]))
                    }
                }



                val myAdapter =
                    MyAdapter(this@ranking, R.layout.ranking_items, list as ArrayList<walkers>?)
                simpleGridView.setAdapter(myAdapter)

            }

            protected override fun doInBackground(vararg params: String?): String? {

                finalResult = httpParse.postRequest(hashMap, HttpURL)
                return finalResult!!
            }
        }

        val userLoginClass = UserLoginClass()
        userLoginClass.execute()
    }


}