package com.example.andproject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MyAdapter  extends ArrayAdapter<walkers> {


    ArrayList<walkers> prodlist= new ArrayList<>();
    public static ArrayList<walkers> cart= new ArrayList<>();

    Context context;

    public MyAdapter(Context context, int textViewResourceId, ArrayList<walkers> objects) {
        super(context, textViewResourceId, objects);
        prodlist = objects;
        this.context=context;
    }


    @Override
    public int getCount() {
        return super.getCount();
    }

    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.ranking_items, null);

        final TextView rank = (TextView) v.findViewById(R.id.rank);
        final TextView  date= (TextView) v.findViewById(R.id.date);
        final TextView name = (TextView) v.findViewById(R.id.name);
        final TextView num_steps = (TextView) v.findViewById(R.id.num_steps);

        if(prodlist.get(position).getRank().equals("1"))
        {
            rank.setTextColor(context.getResources().getColor(R.color.gold));
            name.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }
        else if(prodlist.get(position).getRank().equals("2"))
        {
            rank.setTextColor(context.getResources().getColor(R.color.gray));
            name.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }
        else if(prodlist.get(position).getRank().equals("3"))
        {
            rank.setTextColor(context.getResources().getColor(R.color.bronze));
            name.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }

        else
        {
            rank.setTextColor(context.getResources().getColor(R.color.black));
            name.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }

        //android.text.format.DateFormat.format("dd MMMM yyyy ", prodlist.get(position).getDatee());

        rank.setText(prodlist.get(position).getRank()+".   ");
        date.setText(prodlist.get(position).getDatee());
        name.setText(prodlist.get(position).getName());
        num_steps.setText(prodlist.get(position).getNum_steps()+" Steps");








        return v;




    }




}

