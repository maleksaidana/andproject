package com.example.andproject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class activity_adapter extends ArrayAdapter<activity> {

    ArrayList<activity> prodlist= new ArrayList<>();
    public static ArrayList<activity> cart= new ArrayList<>();

    Context context;




    public activity_adapter(Context context, int textViewResourceId, ArrayList<activity> objects) {
        super(context, textViewResourceId, objects);
        prodlist = objects;
        this.context=context;
    }


    @Override
    public int getCount() {
        return super.getCount();
    }



    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.activity_items, null);


        final TextView name = (TextView) v.findViewById(R.id.name);
        final ImageView icon = (ImageView) v.findViewById(R.id.icon);
        final ImageView add = (ImageView) v.findViewById(R.id.add);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);

                alert.setTitle("ADD "+prodlist.get(position).getName()+" as activity");
                alert.setMessage("\nPlease enter the period in minutes");
                final EditText input = new EditText(context);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                alert.setView(input);

                alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity.instance.AddCaloriesOut(Integer.parseInt(input.getText().toString())*Integer.parseInt(prodlist.get(position).getCal_hour())/60);
                        Toast.makeText(context, "Activity Added", Toast.LENGTH_LONG).show();
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert.show();
            }
        });





        //android.text.format.DateFormat.format("dd MMMM yyyy ", prodlist.get(position).getDatee());

        String a=prodlist.get(position).getName();
        int resID = context.getResources().getIdentifier(a , "drawable", context.getPackageName());

        name.setText(prodlist.get(position).getName());
        icon.setImageResource(resID);








        return v;




    }




}
