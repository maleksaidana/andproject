package com.example.andproject



import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Toast
import com.example.andproject.MainActivity.Companion.instance
import java.util.*
import kotlin.collections.HashMap


internal class MyBroadcastReceiver : BroadcastReceiver() {


    var finalResult: String? = null
    var hashMap: HashMap<String, String> = HashMap()
    var httpParse: HttpParse = HttpParse()
    var HttpURL ="https://bibliobio.000webhostapp.com/andproject/steps.php"


    override fun onReceive(context: Context?, intent: Intent?) {

        var bundle : Bundle?= intent?.extras


       Toast.makeText(context, "Alarm....", Toast.LENGTH_LONG).show()

        Calendar.getInstance().get(Calendar.YEAR).toString()

        UserRegisterFunction(intent?.getStringExtra("email"),Calendar.getInstance().get(Calendar.YEAR).toString()+"-"+(Calendar.getInstance().get(Calendar.MONTH)+1).toString()+"-"+Calendar.getInstance().get(Calendar.DAY_OF_MONTH).toString(), intent?.getStringExtra("steps"),intent?.getStringExtra("cal_in"),intent?.getStringExtra("cal_out"))
        MainActivity.verif=true

        instance.startAlert()



    }

    fun UserRegisterFunction(
        email: String?,
        datee: String?,
        steps: String?,
        cal_in: String?,
        cal_out: String?

    ) {
        class UserRegisterFunctionClass : AsyncTask<String?, Void?, String>() {

            override fun onPreExecute() {
                super.onPreExecute()
            }

            override fun onPostExecute(httpResponseMsg: String) {
                super.onPostExecute(httpResponseMsg)
             //   Toast.makeText(this@MainActivity, httpResponseMsg, Toast.LENGTH_LONG).show()



            }

            override fun doInBackground(vararg params: String?): String {
                hashMap["email"] = params[0].toString()
                hashMap["datee"] = params[1].toString()
                hashMap["steps"] = params[2].toString()
                hashMap["cal_in"] = params[2].toString()
                hashMap["cal_out"] = params[2].toString()
                finalResult = httpParse.postRequest(hashMap, HttpURL)


                return finalResult!!
            }


        }

        val userRegisterFunctionClass = UserRegisterFunctionClass()
        userRegisterFunctionClass.execute(email, datee, steps)
    }


}