package com.example.andproject;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import static android.telephony.AvailableNetworkInfo.PRIORITY_HIGH;
import static android.telephony.AvailableNetworkInfo.PRIORITY_LOW;

public class StepService extends Service {
    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "Channel_Id";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        // do your jobs here

        startForeground();

        return super.onStartCommand(intent, flags, startId);
    }


    private void startForeground() {
        Intent notificationIntent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
       String channel;
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
           channel = createChannel();
       else {
           channel = "";
       }
       int steps = MainActivity.instance.getNumSteps();
       NotificationCompat.Builder mBuilder =new NotificationCompat.Builder(this, channel).setSmallIcon(R.drawable.logo).setContentTitle(getString(R.string.app_name));
       Notification notification = mBuilder.setOngoing(true)
               .setContentTitle(getString(R.string.app_name))
               .setContentText("Your last record was "+steps+" steps!!\uD83D\uDE09 Update it..")
               .setContentIntent(pendingIntent)
               .setAutoCancel(true)
               .build();
       startForeground(NOTIF_ID, notification);
    }

    @NonNull
    @TargetApi(26)
    private synchronized String createChannel() {
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        String name = "step counter location ";
        int importance = NotificationManager.IMPORTANCE_LOW;

        NotificationChannel mChannel = new NotificationChannel("step counter channel", name, importance);

        mChannel.enableLights(true);
        mChannel.setLightColor(Color.BLUE);
        if (mNotificationManager != null) {
            mNotificationManager.createNotificationChannel(mChannel);
        } else {
            stopSelf();
        }
        return "step counter channel";
    }
}
