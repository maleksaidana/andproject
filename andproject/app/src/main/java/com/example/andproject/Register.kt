package com.example.andproject

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.andproject.AddActivity
import com.example.andproject.ScanProduct
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.Email
import kotlinx.android.synthetic.main.activity_register.Password
import kotlinx.android.synthetic.main.header_lyt.*


@Suppress("DEPRECATION")
class Register : Activity() {


    var F_Name_Holder: String? = null
    var L_Name_Holder:kotlin.String? = null
    var HeightHolder:kotlin.String? = null
    var WeightHolder:kotlin.String? = null
    var AgeHolder:kotlin.String? = null
    var EmailHolder:kotlin.String? = null
    var PasswordHolder:kotlin.String? = null

    var finalResult: String? = null
    var HttpURL ="https://bibliobio.000webhostapp.com/andproject/UserRegistration.php"
    var CheckEditText: Boolean? = null
    var progressDialog: ProgressDialog? = null
    var hashMap: HashMap<String, String> = HashMap()
    var httpParse: HttpParse = HttpParse()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(com.example.andproject.R.layout.activity_register)


        /*menu*/
        val usermail: SharedPreferences = getSharedPreferences("user", 0)
        if (usermail.getString("mail", "")!="") {
            login.setImageResource(R.drawable.profile);
        }


        home.setOnClickListener  {startActivity(Intent(this, MainActivity::class.java))}
        AddActivity.setOnClickListener  {startActivity(Intent(this, com.example.andproject.AddActivity::class.java))}
        ScanProduct.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ScanProduct::class.java))}
        rankingbtn.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ranking::class.java))}
        login.setOnClickListener  {
            startActivity(Intent(this, Login::class.java))
            if (usermail.getString("mail", "")!=""){
                val intent = Intent(this, Profile::class.java)
                startActivity(intent)
            }
            else {
                startActivity(Intent(this, Login::class.java))
            }
        }
        /*menu*/

        register.setOnClickListener(){


                CheckEditTextIsEmptyOrNot()
                if (CheckEditText!!) {

                    UserRegisterFunction(F_Name_Holder, L_Name_Holder, HeightHolder,WeightHolder,AgeHolder,EmailHolder, PasswordHolder)


                } else {

                    Toast.makeText(
                        this,
                        "Please fill all form fields.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }


        log_in.setOnClickListener(){
            startActivity(Intent(this, Login::class.java))

        }







    }
    override fun onPause() {
        super.onPause()
        startService(Intent(this, StepService::class.java))

    }

    override fun onResume() {
        super.onResume()
        stopService(Intent(this, StepService::class.java))

    }

    override fun onDestroy() {
        super.onDestroy()
        startService(Intent(this, StepService::class.java))
    }


    fun CheckEditTextIsEmptyOrNot() {
        F_Name_Holder = First_Name.text.toString()
        L_Name_Holder = Last_Name.text.toString()
        HeightHolder = Height.text.toString()
        WeightHolder = Weight.text.toString()
        AgeHolder = Age.text.toString()
        EmailHolder = Email.text.toString()
        PasswordHolder = Password.text.toString()
        CheckEditText =
            if (TextUtils.isEmpty(F_Name_Holder) || TextUtils.isEmpty(L_Name_Holder) || TextUtils.isEmpty(
                    EmailHolder
                ) || TextUtils.isEmpty(PasswordHolder)|| TextUtils.isEmpty(HeightHolder)|| TextUtils.isEmpty(WeightHolder)|| TextUtils.isEmpty(AgeHolder)
            ) {
                false
            } else {
                true
            }
    }

    fun UserRegisterFunction(
        F_Name: String?,
        L_Name: String?,
        Height: String?,
        Weight: String?,
        Age: String?,
        email: String?,
        password: String?
    ) {
        class UserRegisterFunctionClass : AsyncTask<String?, Void?, String>() {

            override fun onPreExecute() {
                super.onPreExecute()
            }

            override fun onPostExecute(httpResponseMsg: String) {
                super.onPostExecute(httpResponseMsg)
                Toast.makeText(this@Register, httpResponseMsg, Toast.LENGTH_LONG).show()



            }

            override fun doInBackground(vararg params: String?): String {
                hashMap["f_name"] = params[0].toString()
                hashMap["L_name"] = params[1].toString()
                hashMap["height"] = params[2].toString()
                hashMap["weight"] = params[3].toString()
                hashMap["age"] = params[4].toString()
                hashMap["email"] = params[5].toString()
                hashMap["password"] = params[6].toString()
                finalResult = httpParse.postRequest(hashMap, HttpURL)
                Log.e("salah","lol")

                return finalResult!!
            }


        }

        val userRegisterFunctionClass = UserRegisterFunctionClass()
        userRegisterFunctionClass.execute(F_Name, L_Name,Height,Weight,Age, email, password)
    }


}