package com.example.andproject



import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.widget.Toast
import com.example.andproject.MainActivity.Companion.instance
import kotlinx.android.synthetic.main.activity_main.*


internal class MyBroadcastReceiverForCalories : BroadcastReceiver() {


    override fun onReceive(context: Context?, intent: Intent?) {

        //Toast.makeText(context, "Alarm....", Toast.LENGTH_LONG).show()
        MainActivity.verifCal=true
        MainActivity.timeCalories+=220

        if(MainActivity.CaloriesTime==24)
        {
            MainActivity.CaloriesTime=0
            MainActivity.timeCalories=0
            instance.numCaloriesOut = 0
            instance.numCaloriesIn = 0
            instance.numSteps = 0
            instance.reset_calories_in()
            instance.reset_calories_out()
            instance.reset_steps()
        }
        MainActivity.CaloriesTime += 3
        instance.startTime()

    }




}