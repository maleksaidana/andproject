package com.example.andproject;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class simpleNotification extends Service {

    private static final int NOTIF_ID = 2;
    private static final String NOTIF_CHANNEL_ID = "Channel_Id";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    public int onStartCommand(Intent intent, int flags, int startId){

        // do your jobs here

        startForeground();

        return super.onStartCommand(intent, flags, startId);
    }


    private void startForeground() {
        Intent notificationIntent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        String channel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            channel = createChannel();
        else {
            channel = "";
        }
        int achivedSteps = MainActivity.instance.getAchivedSteps();


        NotificationCompat.Builder mBuilder =new NotificationCompat.Builder(this, channel).setSmallIcon(R.drawable.logo).setContentTitle(getString(R.string.app_name));
        Notification notification = mBuilder.setContentTitle(getString(R.string.app_name))
                .setContentText("congratulations you have reached "+achivedSteps+" steps so far!!")
                .setContentIntent(pendingIntent)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.BLUE, 3000, 3000)
                .setAutoCancel(true)
                .setSound(Uri.parse("uri://sadfasdfasdf.mp3"))
                .build();
        startForeground(NOTIF_ID, notification);


    }

    @NonNull
    @TargetApi(26)
    private synchronized String createChannel() {
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        String name = "Achivements notifications channel";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        NotificationChannel mChannel = new NotificationChannel("Achivements notifications channel", name, importance);

        mChannel.enableLights(true);
        mChannel.enableVibration(true);

        mChannel.setLightColor(Color.BLUE);
        if (mNotificationManager != null) {
            mNotificationManager.createNotificationChannel(mChannel);
        } else {
            stopSelf();
        }
        return "Achivements notifications channel";
    }
}
