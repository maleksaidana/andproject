package com.example.andproject

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.andproject.AddActivity
import com.example.andproject.ScanProduct
import kotlinx.android.synthetic.main.activity_product_info.*
import kotlinx.android.synthetic.main.header_lyt.*
import com.example.andproject.MainActivity.Companion.instance
import java.util.*

class product_info : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_info)


      //  htmll.text=intent?.getStringExtra("html")

        name.text= intent?.getStringExtra("html")
        /*menu*/
        val usermail: SharedPreferences = getSharedPreferences("user", 0)
        if (usermail.getString("mail", "")!="") {
            login.setImageResource(R.drawable.profile);
        }


        home.setOnClickListener  {startActivity(Intent(this, MainActivity::class.java))}
        AddActivity.setOnClickListener  {startActivity(Intent(this, com.example.andproject.AddActivity::class.java))}
        ScanProduct.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ScanProduct::class.java))}
        rankingbtn.setOnClickListener  {startActivity(Intent(this, ranking::class.java))}
        login.setOnClickListener  {
            startActivity(Intent(this, Login::class.java))
            if (usermail.getString("mail", "")!=""){
                val intent = Intent(this, Profile::class.java)
                startActivity(intent)
            }
            else {
                startActivity(Intent(this, Login::class.java))
            }
        }
        /*menu*/

        add.setOnClickListener(){
            if(name.text!="Add a product") {
                instance.AddCaloriesIn((100..300).random())
                startActivity(Intent(this, MainActivity::class.java))
            }
            else
                Toast.makeText(this, "Unkwon Product, Scan Another One", Toast.LENGTH_LONG).show()
        }


    }
}