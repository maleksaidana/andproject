package com.example.andproject

import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.andproject.AddActivity
import com.example.andproject.ScanProduct
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile.tv_calories_in
import kotlinx.android.synthetic.main.activity_profile.tv_calories_out
import kotlinx.android.synthetic.main.activity_profile.tv_steps
import kotlinx.android.synthetic.main.activity_profile.tvv_calories_in
import kotlinx.android.synthetic.main.activity_profile.tvv_calories_out
import kotlinx.android.synthetic.main.activity_profile.tvv_steps
import kotlinx.android.synthetic.main.header_lyt.*
import java.util.*
import kotlin.collections.HashMap

class Profile : AppCompatActivity() {

    var hashMap: HashMap<String, String> = HashMap()
    var httpParse = HttpParse()
    var finalResult: String? = null
    var numSteps:Int=0
    var numCaloriesIn:Int=0
    var numCaloriesOut:Int=0
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "mindorks-welcome"
    var HttpURL = "https://bibliobio.000webhostapp.com/andproject/historique.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.andproject.R.layout.activity_profile)


        /*menu*/
        val usermail: SharedPreferences = getSharedPreferences("user", 0)

        val p: SharedPreferences = getSharedPreferences("person", 0)
        val history: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        numSteps = history.getInt("steps", 0)
        numCaloriesIn = history.getInt("caloriesin", 0)
        numCaloriesOut = history.getInt("caloriesout", 0)
        tvv_steps.text=""+numSteps+"/10000 Steps"
        tv_steps.setProgress(numSteps)

        tvv_calories_in.text=""+numCaloriesIn+"\nCalories"
        tv_calories_in.setProgress(numCaloriesIn)

        tvv_calories_out.text=""+numCaloriesOut+"\nCalories"
        tv_calories_out.setProgress(numCaloriesOut)

        val c1: Calendar = Calendar.getInstance()

        date.text=c1.get(Calendar.YEAR).toString()+"-"+((c1.get(Calendar.MONTH))+1).toString()+"-"+c1.get(Calendar.DAY_OF_MONTH).toString()

        if (usermail.getString("mail", "")!="") {
            login.setImageResource(R.drawable.profile);
        }


        home.setOnClickListener  {startActivity(Intent(this, MainActivity::class.java))}
        AddActivity.setOnClickListener  {startActivity(Intent(this, com.example.andproject.AddActivity::class.java))}
        ScanProduct.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ScanProduct::class.java))}
        rankingbtn.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ranking::class.java))}
        login.setOnClickListener  {
            startActivity(Intent(this, Login::class.java))
            if (usermail.getString("mail", "")!=""){
                val intent = Intent(this, Profile::class.java)
                startActivity(intent)
            }
            else {
                startActivity(Intent(this, Login::class.java))
            }
        }
        /*menu*/



        backbtn.setOnClickListener(){

           UserLoginFunction(usermail.getString("mail", ""), date.text.toString(),"-")



        }

        nextbtn.setOnClickListener(){

            UserLoginFunction(usermail.getString("mail", ""), date.text.toString(),"+")

        }



        /*EmailHolder = intent.getStringExtra("UserEmail")
        EmailShow.setText(EmailHolder)*/

        name.setText(p.getString("firstname", "")+" "+p.getString("lastname", ""))
        height.setText((p.getString("height", "")?.toFloat()?.div(100)).toString()+"m")
        weight.setText(p.getString("weight", "")+"Kg")
         age.setText(p.getString("age", "")+"yo")
        rank.setText("#"+p.getString("rank", ""))
        days.setText(p.getString("days", ""))
        steps.setText(p.getString("steps", "None"))



        LogOut.setOnClickListener(){

            finish()

           
           usermail.edit().remove("mail").commit()
            p.edit().remove("firstname").commit()
            p.edit().remove("lastname").commit()
            p.edit().remove("height").commit()
            p.edit().remove("weight").commit()
            p.edit().remove("age").commit()
            MainActivity.verif=false

            val intent = Intent(this, Login::class.java)
            startActivity(intent)
            Toast.makeText(this, "Log Out Successfully", Toast.LENGTH_LONG).show()
        }


    }
    override fun onPause() {
        super.onPause()
        startService(Intent(this, StepService::class.java))

    }

    override fun onResume() {
        super.onResume()
        stopService(Intent(this, StepService::class.java))

    }

    override fun onDestroy() {
        super.onDestroy()
        startService(Intent(this, StepService::class.java))
    }

    override fun onBackPressed() {

    }



    fun UserLoginFunction(email: String?, datee: String?, type: String?) {
        class UserLoginClass :
            AsyncTask<String?, Void?, String>() {
            override fun onPreExecute() {
                super.onPreExecute()
                //progressDialog =
                // ProgressDialog.show(this, "Loading Data", null, true, true)
            }

            override fun onPostExecute(httpResponseMsg: String) {
                super.onPostExecute(httpResponseMsg)
                //progressDialog!!.dismiss()
                if (httpResponseMsg.equals("false", ignoreCase = true)) {

                    Toast.makeText(this@Profile, "No Date", Toast.LENGTH_LONG)
                        .show()

                    if(type!="-"){
                        val p: SharedPreferences = getSharedPreferences("person", 0)
                        val history: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
                        numSteps = history.getInt("steps", 0)
                        numCaloriesIn = history.getInt("caloriesin", 0)
                        numCaloriesOut = history.getInt("caloriesout", 0)
                        tvv_steps.text=""+numSteps+"/10000 Steps"
                        tv_steps.setProgress(numSteps)

                        tvv_calories_in.text=""+numCaloriesIn+"\nCalories"
                        tv_calories_in.setProgress(numCaloriesIn)

                        tvv_calories_out.text=""+numCaloriesOut+"\nCalories"
                        tv_calories_out.setProgress(numCaloriesOut)

                        val c1: Calendar = Calendar.getInstance()

                        date.text=c1.get(Calendar.YEAR).toString()+"-"+((c1.get(Calendar.MONTH))+1).toString()+"-"+c1.get(Calendar.DAY_OF_MONTH).toString()
                    }


                } else {




                    date.text=httpResponseMsg.split("?").toTypedArray()[1]

                    tvv_steps.text=httpResponseMsg.split("?").toTypedArray()[2]+"/10000 Steps"
                    tv_steps.setProgress((httpResponseMsg.split("?").toTypedArray()[2]).toInt())

                    tvv_calories_in.text=httpResponseMsg.split("?").toTypedArray()[3]+"\nCalories"
                    tv_calories_in.setProgress((httpResponseMsg.split("?").toTypedArray()[3]).toInt())

                    tvv_calories_out.text=httpResponseMsg.split("?").toTypedArray()[4]+"\nCalories"
                    tv_calories_out.setProgress((httpResponseMsg.split("?").toTypedArray()[4]).toInt())

                }
            }

            protected override fun doInBackground(vararg params: String?): String? {
                hashMap["email"] = params[0].toString()
                hashMap["datee"] = params[1].toString()
                hashMap["type"] = params[2].toString()
                finalResult = httpParse.postRequest(hashMap, HttpURL)
                return finalResult!!
            }
        }

        val userLoginClass = UserLoginClass()
        userLoginClass.execute(email, datee,type)
    }


}