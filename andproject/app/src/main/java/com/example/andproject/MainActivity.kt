package com.example.andproject

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Window
import androidx.annotation.RequiresApi
import androidx.core.text.HtmlCompat
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.header_lyt.*
import kotlinx.android.synthetic.main.ranking_items.*
import java.lang.Math.round
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.LONG


class MainActivity : Activity(), SensorEventListener, StepListener {
    private var simpleStepDetector:StepDetector?=null;
    private var sensorManager:SensorManager?=null;
    private var accel:Sensor?=null;
    private val TEXT_NUM_STEPS:String = "/10000 Steps";
    private val TEXT_NUM_CALORIES:String = "Calories";
    private val appLink:String = "playstore";
    var numSteps:Int=0
    var numCaloriesIn:Int=0
    var numCaloriesOut:Int=0
    var achivedSteps:Int=0
    internal var numCalories:Int=0
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "mindorks-welcome"
    private var message:String=""

    companion object {
        var verif: Boolean=true
        var verifCal: Boolean=true
        var CaloriesTime:Int=0
        var timeCalories:Int=0
        lateinit var instance:MainActivity
    }
    init {
        instance = this
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(com.example.andproject.R.layout.activity_main);
        tv_steps.setProgress(0);
        tv_steps.setSecondaryProgress(10000);
        tv_calories_in.setProgress(0);
        tv_calories_in.setSecondaryProgress(5000);
        tv_calories_out.setProgress(0);
        tv_calories_out.setSecondaryProgress(5000);

        val c1: Calendar = Calendar.getInstance()

        today.text=c1.get(Calendar.DAY_OF_MONTH).toString()+" "+(c1.getDisplayName(Calendar.MONTH,LONG,
            Locale.getDefault())).toString()+" "+c1.get(Calendar.YEAR).toString()



        val stepcount: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        val date: SharedPreferences = getSharedPreferences("date",0)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accel = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        simpleStepDetector = StepDetector()
        simpleStepDetector!!.registerListener(this)
        numSteps = stepcount.getInt("steps", 0)
        numCaloriesIn = stepcount.getInt("caloriesin", 0)
        numCaloriesOut = stepcount.getInt("caloriesout", 0)
        tvv_steps.text =  ""+numSteps+TEXT_NUM_STEPS
        tv_steps.setProgress(numSteps)
        tvv_calories_in.text =  ""+numCaloriesIn+"\n"+TEXT_NUM_CALORIES
        tv_calories_in.setProgress(numCaloriesIn)
        tvv_calories_out.text =  ""+numCaloriesOut+"\n"+TEXT_NUM_CALORIES
        tv_calories_out.setProgress(numCaloriesOut)
        message="Hello,<br/>Look how many steps I've walked today <b><i><u><span style='color:red'>"+numSteps+" Steps!!\uD83D\uDE03</span></u></i></b><br/>Also the number of calories I've burned so far...<b><i><u><span style='color:red'>"+numCalories+" Calories!!\uD83D\uDE0D\uD83D\uDE0D</span></u></i></b><br/>Join the club by installing this app and compete with other people in a healthy way \uD83D\uDC97\uD83D\uDC97\uD83D\uDC97<br/>"+appLink+"<br/>Thank you!!"










        /*menu*/
        val usermail: SharedPreferences = getSharedPreferences("user", 0)
        if (usermail.getString("mail", "")!="") {
            login.setImageResource(com.example.andproject.R.drawable.profile);
        }
        else{
            verif=false
        }


        home.setOnClickListener  {startActivity(Intent(this, MainActivity::class.java))}
        AddActivity.setOnClickListener  {startActivity(Intent(this, com.example.andproject.AddActivity::class.java))}
        ScanProduct.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ScanProduct::class.java))}
        rankingbtn.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ranking::class.java))}
        loadContacts.setOnClickListener  {sendSMS(message)}
        login.setOnClickListener  {
            startActivity(Intent(this, Login::class.java))
            if (usermail.getString("mail", "")!=""){
                val intent = Intent(this, Profile::class.java)
                startActivity(intent)
            }
            else {
                startActivity(Intent(this, Login::class.java))
            }
        }
        /*menu*/


        sensorManager!!.registerListener(
                    this@MainActivity,
                    accel,
                    SensorManager.SENSOR_DELAY_FASTEST
                )


        val c: Calendar = Calendar.getInstance()


        startAlert()
        startTime()

    }



    private fun sendSMS(message:String)
    {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.setType("text/html")
        intent.data = Uri.parse("smsto:" + Uri.encode("")).buildUpon().appendQueryParameter("body",HtmlCompat.fromHtml(message, HtmlCompat.FROM_HTML_MODE_LEGACY).toString()).build();
        startActivity(intent)
        //request permission
        /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(
                    Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.SEND_SMS),
                   PERMISSIONS_REQUEST_SEND_SMS
                )
                //callback onRequestPermissionsResult
            } else {
                //val smgr: SmsManager = SmsManager.getDefault()
               // smgr.sendTextMessage(phoneNumber, null, message, null, null)
                sendSMS(phoneNumber,message)

            }*/
    }
    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector!!.updateAccel(
                event.timestamp, event.values[0], event.values[1], event.values[2]
            )
        }
    }

    override fun onPause() {
        super.onPause()
        startService(Intent(this, StepService::class.java))
        stopService(Intent(this, simpleNotification::class.java))
    }

    override fun onResume() {
        super.onResume()
        stopService(Intent(this, StepService::class.java))
        stopService(Intent(this, simpleNotification::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        startService(Intent(this, StepService::class.java))
        stopService(Intent(this, simpleNotification::class.java))

    }

    override fun onSaveInstanceState(outState: Bundle) {
        // Make sure to call the super method so that the states of our views are saved
        super.onSaveInstanceState(outState)
        outState.putInt("steps",numSteps)
        outState.putInt("caloriesin",numCaloriesIn)
        outState.putInt("caloriesout",numCaloriesOut+ timeCalories)

    }


    override fun step(timeNs: Long) {

        val pref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)

        numSteps++
        message="Hello,<br/>Look how many steps I've walked today <b><i><u><span style='color:red'>"+numSteps+" Steps!!\uD83D\uDE03</span></u></i></b><br/>Also the number of calories I've burned so far...<b><i><u><span style='color:red'>"+numCalories+" Calories!!\uD83D\uDE0D\uD83D\uDE0D</span></u></i></b><br/>Join the club by installing this app and compete with other people in a healthy way \uD83D\uDC97\uD83D\uDC97\uD83D\uDC97<br/>"+appLink+"<br/>Thank you!!"
        tvv_steps.text = ""+numSteps+TEXT_NUM_STEPS
        tv_steps.setProgress(numSteps)
        tvv_calories_out.text = ""+pref.getInt("caloriesout",0)+"\n"+TEXT_NUM_CALORIES
        tv_calories_out.setProgress(pref.getInt("caloriesout",0))


        var mEditor: Editor = pref.edit()
            mEditor.putInt("steps", numSteps)
            mEditor.putInt("caloriesout", round(numSteps* 0.045).toInt()+pref.getInt("timecalories",0))
           mEditor.commit()

        if (numSteps==500)
        {
            achivedSteps=500
            startService(Intent(this, simpleNotification::class.java))

        }
        else if(numSteps==2500)
        {
            achivedSteps=2500
            startService(Intent(this, simpleNotification::class.java))
        }
        else if(numSteps==5000)
        {
            achivedSteps=5000
            startService(Intent(this, simpleNotification::class.java))
        }
        else if(numSteps==10000)
        {
            achivedSteps=10000
            startService(Intent(this, simpleNotification::class.java))
        }
    }


    fun startTime() {
        if(verifCal==true) {
            val pref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            var mEditor: Editor = pref.edit()
            mEditor.putInt("timecalories",timeCalories)
            mEditor.commit()

            val c: Calendar = Calendar.getInstance()
            verifCal=false
            c.set(Calendar.HOUR_OF_DAY, CaloriesTime)
            c.set(Calendar.MINUTE, 0)
            c.set(Calendar.SECOND, 0)
            c.set(Calendar.MILLISECOND, 0)
            val howMany: Long = c.getTimeInMillis() - System.currentTimeMillis()
            val intent = Intent(this, MyBroadcastReceiverForCalories::class.java)

            val pendingIntent =
                PendingIntent.getBroadcast(this.applicationContext, 2343242, intent, 0)
            val alarmManager =
                getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager[AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + howMany] = pendingIntent
            //Toast.makeText(this, CaloriesTime.toString(), Toast.LENGTH_LONG).show()

        }
    }
     fun startAlert() {
        if(verif==true) {
            val usermail: SharedPreferences = getSharedPreferences("user", 0)
            val stepcount: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            verif=false
            val c: Calendar = Calendar.getInstance()

            c.set(Calendar.HOUR_OF_DAY, 23)
            c.set(Calendar.MINUTE, 59)
            c.set(Calendar.SECOND, 59)
            c.set(Calendar.MILLISECOND, 999)
            val howMany: Long = c.getTimeInMillis() - System.currentTimeMillis()
            val intent = Intent(this, MyBroadcastReceiver::class.java)
            intent.putExtra("steps", stepcount.getInt("steps", 0).toString())
            intent.putExtra("email", usermail.getString("mail", ""))
            intent.putExtra("cal_in", stepcount.getInt("caloriesin", 0).toString())
            intent.putExtra("cal_out", stepcount.getInt("caloriesout", 0).toString())

            val pendingIntent =
                PendingIntent.getBroadcast(this.applicationContext, 234324243, intent, 0)
            val alarmManager =
                getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager[AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + howMany] = pendingIntent
            //Toast.makeText(this, c.get(Calendar.DAY_OF_MONTH).toString(), Toast.LENGTH_LONG).show()
        }
    }
    fun AddCaloriesIn(number_of_calories:Int)
    {
        val pref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)

        numCaloriesIn = pref.getInt("caloriesin", 0)
        var new_calories:Int=number_of_calories+numCaloriesIn
        var mEditor: Editor = pref.edit()
        mEditor.putInt("caloriesin",new_calories)
        mEditor.commit()
        tvv_calories_in.text = ""+new_calories+"\n"+TEXT_NUM_CALORIES
        tv_calories_in.setProgress(new_calories)
    }
    fun AddCaloriesOut(number_of_calories:Int)
    {
        val pref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)

         numCaloriesOut = pref.getInt("caloriesout", 0)
         CaloriesTime = pref.getInt("timecalories", 0)
        var new_calories:Int=number_of_calories+numCaloriesOut
        var mEditor: Editor = pref.edit()
        mEditor.putInt("timecalories",new_calories)
        mEditor.putInt("caloriesout",new_calories)
        mEditor.commit()
        numCaloriesOut = pref.getInt("caloriesout", 0)
        tvv_calories_out.text = ""+numCaloriesOut+"\n"+TEXT_NUM_CALORIES
        tv_calories_out.setProgress(numCaloriesOut)
    }
    fun reset_calories_in()
    {
        val pref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        var mEditor: Editor = pref.edit()
        mEditor.putInt("caloriesin",0)
        mEditor.commit()
        numCaloriesIn = 0
        tvv_calories_in.text = ""+numCaloriesIn+"\n"+TEXT_NUM_CALORIES
        tv_calories_in.setProgress(numCaloriesIn)
    }
    fun reset_calories_out()
    {
        val pref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        this.numCaloriesOut = 0
        CaloriesTime = 0
        this.numSteps = 0
        var mEditor: Editor = pref.edit()
        mEditor.putInt("timecalories",0)
        mEditor.putInt("caloriesout",0)
        mEditor.commit()

        tvv_calories_out.text = ""+numCaloriesOut+"\n"+TEXT_NUM_CALORIES
        tv_calories_out.setProgress(numCaloriesOut)
    }
    fun reset_steps()
    {
        val pref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        var mEditor: Editor = pref.edit()
        mEditor.putInt("steps",0)
        mEditor.commit()
        this.numSteps = 0
        tvv_steps.text = ""+numSteps+TEXT_NUM_STEPS
        tv_steps.setProgress(numSteps)
    }



}
