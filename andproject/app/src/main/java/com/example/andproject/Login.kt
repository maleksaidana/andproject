package com.example.andproject

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.view.Window
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.header_lyt.*


class Login : Activity() {

    var PasswordHolder: String? = null
    var EmailHolder:kotlin.String? = null
    var finalResult: String? = null
    var HttpURL = "https://bibliobio.000webhostapp.com/andproject/UserLogin.php"
    var CheckEditText: Boolean? = null
    var progressDialog: ProgressDialog? = null
    var hashMap: HashMap<String, String> = HashMap()
    var httpParse = HttpParse()
    val UserEmail = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(com.example.andproject.R.layout.activity_login);


        /*menu*/
        val usermail: SharedPreferences = getSharedPreferences("user", 0)
        if (usermail.getString("mail", "")!="") {
            login.setImageResource(R.drawable.profile);
        }


        home.setOnClickListener  {startActivity(Intent(this, MainActivity::class.java))}
        AddActivity.setOnClickListener  {startActivity(Intent(this, com.example.andproject.AddActivity::class.java))}
        ScanProduct.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ScanProduct::class.java))}
        rankingbtn.setOnClickListener  {startActivity(Intent(this, com.example.andproject.ranking::class.java))}
        login.setOnClickListener  {
            startActivity(Intent(this, Login::class.java))
            if (usermail.getString("mail", "")!=""){
                val intent = Intent(this, Profile::class.java)
                startActivity(intent)
            }
            else {
                startActivity(Intent(this, Login::class.java))
            }
        }
        /*menu*/


        LogIn.setOnClickListener() {

                CheckEditTextIsEmptyOrNot()
                if (CheckEditText!!) {
                    UserLoginFunction(EmailHolder, PasswordHolder)
                } else {
                    Toast.makeText(
                        this,
                        "Please fill all form fields.",
                        Toast.LENGTH_LONG
                    ).show()
                }

        }
        SignIn.setOnClickListener() {
            startActivity(Intent(this, Register::class.java))
        }

    }
    override fun onPause() {
        super.onPause()
        startService(Intent(this, StepService::class.java))

    }

    override fun onResume() {
        super.onResume()
        stopService(Intent(this, StepService::class.java))

    }

    override fun onDestroy() {
        super.onDestroy()
        startService(Intent(this, StepService::class.java))
    }


    fun CheckEditTextIsEmptyOrNot() {
        EmailHolder = Email.text.toString()
        PasswordHolder = Password.text.toString()
        CheckEditText = !(TextUtils.isEmpty(EmailHolder) || TextUtils.isEmpty(PasswordHolder))
    }

    fun UserLoginFunction(email: String?, password: String?) {
        class UserLoginClass :
            AsyncTask<String?, Void?, String>() {
            override fun onPreExecute() {
                super.onPreExecute()
                //progressDialog =
                   // ProgressDialog.show(this, "Loading Data", null, true, true)
            }

            override fun onPostExecute(httpResponseMsg: String) {
                super.onPostExecute(httpResponseMsg)
                //progressDialog!!.dismiss()
                if (httpResponseMsg.equals("false", ignoreCase = true)) {

                    Toast.makeText(this@Login, "Invalid Username or Password Please Try Again", Toast.LENGTH_LONG)
                        .show()


                } else {


                    finish()
                    MainActivity.verif=true
                    val useremail: SharedPreferences = getSharedPreferences("user", 0)
                    var mEditor: SharedPreferences.Editor = useremail.edit()
                    mEditor.putString("mail", email)
                    mEditor.commit()

                   var p = person(httpResponseMsg.split("?").toTypedArray()[0],httpResponseMsg.split("?").toTypedArray()[1],httpResponseMsg.split("?").toTypedArray()[2],httpResponseMsg.split("?").toTypedArray()[3],httpResponseMsg.split("?").toTypedArray()[4],httpResponseMsg.split("?").toTypedArray()[5],httpResponseMsg.split("?").toTypedArray()[7],httpResponseMsg.split("?").toTypedArray()[8],httpResponseMsg.split("?").toTypedArray()[6])

                    val person: SharedPreferences = getSharedPreferences("person", 0)
                    var pEditor: SharedPreferences.Editor = person.edit()
                    pEditor.putString("firstname", p.firstname)
                    pEditor.putString("lastname", p.lastname)
                    pEditor.putString("height", p.height)
                    pEditor.putString("weight", p.weight)
                    pEditor.putString("age", p.age)
                    pEditor.putString("steps", p.steps)
                    pEditor.putString("days", p.days)
                    pEditor.putString("rank", p.rank)
                    pEditor.commit()

                    val intent = Intent(this@Login, Profile::class.java)
                    startActivity(intent)

                }
            }

            protected override fun doInBackground(vararg params: String?): String? {
                hashMap["email"] = params[0].toString()
                hashMap["password"] = params[1].toString()
                finalResult = httpParse.postRequest(hashMap, HttpURL)
                return finalResult!!
            }
        }

        val userLoginClass = UserLoginClass()
        userLoginClass.execute(email, password)
    }



}
